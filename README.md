<p align="center">
    <img src="https://gitlab.com/uploads/-/system/project/avatar/18816119/doctrine_logo.png" height=100 />
</p>

<h1 align=center>Doctrine Extensions</h1>

<br/>

This package contains extensions for Doctrine ORM **ONLY** *(not MongoDB ODM, sorry about that)*,
that offers new functionality or tools to use Doctrine more efficiently.<br/>
These behaviors can be easily attached to the event system of Doctrine and handle the records being flushed
in a behavioral way.
<br/><br/>
This package is based on [Gedmo Doctrine Behavioral Extensions](https://github.com/Atlantic18/DoctrineExtensions).
<br/>
Note that attributes in this repo use the Gedmo namespace:
`Gedmo\Mapping`



## Table of Contents

1. [Installation](#installation)
2. [Sequenceable Extension](#sequenceable-extension)
3. [Ownable Extension](#ownable-extension)
4. [Settingsable Extension](#settingsable-extension)
5. [Origin Extension](#origin-extension)
6. [Post Flush Subscriber Abstraction](#post-flush-subscriber-abstraction)
7. [Auto Validator](#auto-validator)


<br/><br/>
## Installation

### Installation with composer

This is installable via [Composer](https://getcomposer.org/):

    composer config repositories.get-repo/doctrine-extensions git https://gitlab.com/get-repo/doctrine-extensions.git
    composer require get-repo/doctrine-extensions

### Enable the bundle

In *config/bundles.php* if not already done by Symfony already
```php
<?php
return [
    ...
    GetRepo\DoctrineExtension\GetRepoDoctrineExtensionBundle::class => ['all' => true],
];
```



<br/><br/><br/>


## Sequenceable Extension

**Sequenceable** behavior will automate a sequence for a field.

Features:

- Automatic sequence number
- ORM support using listener
- Specific attributes for properties, and no interface required
- Can react to specific property or relation changes to specific value
- Can be nested with other behaviors

#### Options
- **step** - sequences step *(e.g: 10, 20, 30 etc..)*
- **startAt** - sequences starts at this number (default = 1)
- **grouping** - Grouping id to behave like a batch of sequences (default = NULL)
- **format** - Custom format of the sequence *(e.g: 'SEQ-{!}')*
- **formatWithDate** - Try to convert format to date
- **service** - Custom service to handle the sequence (must implement the `SequenceableServiceInterface`)


#### Attribute mapping example
```php
#[Sequenceable(startAt: 100, step: 10)]
#[ORM\Column(type: 'integer')]
private int $counter;

// will create the sequence : 100, 110, 120, 130, 140, 150 ...
```


<br/><br/><br/>


## Ownable Extension

**Ownable** behavior will add an owner for an entity.

Features:

- ORM support using filter for repository
- ORM support using listener
- Specific attribute for associations, and no interface required
- Can be nested with other behaviors

#### Options
- **roleAdmin** - Defines the admin role, the admin can see all entities.


#### Attribute mapping example
```php
#[Ownable]
#[ORM\ManyToOne(targetEntity: User::class)]
private ?UserEntity $owner = null,
```


<br/><br/><br/>


## Settingsable Extension

**Settingsable** behavior will create an array, automatically validated by the [Symfony Config Component](https://symfony.com/doc/current/components/config.html).<br/>

Features:

- ORM support using listener
- Create your own config and automatically validated
- Specific attribute for properties, and no interface required
- Can be nested with other behaviors
- Contains a trait

#### Options
- **definitionMethod** - (optional) The name of the method for definitions (see examples below)

#### Attribute mapping example
```php
// use trait in you entity
use SettingsableTrait;

// define settings definitions
public static function getSettingDefinitions(): array
{
    return [
        'is_mandatory' => ['mandatory' => false],
        'is_not_mandatory' => ['mandatory' => true],
        'default_allowed_types' => ['default' => false, 'allowed_types' => ['boolean']],
        'default_allowed_values' => ['default' => 1, 'allowed_values' => [1, 2, 3]],
    ];
}
```


<br/><br/><br/>


## Origin Extension

**Origin** behavior will save the context origin from a request or script.

Features:

- Automatic context guess.
- ORM support using listener
- Specific attribute for properties, and no interface required
- A trait to use the default property
- Can be nested with other behaviors

#### Origin annotations:
- **@Gedmo\Mapping\Origin;** this annotation tells that this column is for the origin.
  It updates this column on pre-persist.

No attribute options available.<br/>
In *config/packages/getrepo_doctrine_extension.yaml* you can set your own custom rules:
```yaml
format: 'custom_format_{context}@{value}' # custom format with {context} {value} placeholders
rules:
    cli:
        # if matched, then will set origin = cli@phpunit
        phpunit: 'argv[0] matches "#phpunit#"'
        # if matched, then will set origin = cli@symfony_command
        symfony_command: 'argv[0] matches "#bin/console#"'
    web:
        # if matched, then will set origin = web@api
        api: 'request.getPathInfo() matches "#^/api/#"'
```

#### Attribute mapping example
```php
<?php
namespace My\App;

use Doctrine\ORM\Mapping as ORM;
use GetRepo\DoctrineExtension\Origin\Traits\OriginableEntity;

/**
 * @ORM\Entity
 */
class OriginExample
{
    // use the trait
    use OriginableEntity;
}
```


<br/><br/><br/>


## One Row Extension

**OneRow** behavior will always make sure the table contains only one row.

Features:

- ORM support using listener
- Specific attribute for class, and no interface required
- Can be nested with other behaviors

#### Options
- **instantiate** - Auto instantiate a new entity when the table is empty.

#### Attribute mapping example
```php
#[ORM\Table(name: 'example')]
#[ORM\Entity]
#[OneRow]
class OneRowTestEntity
{
    // ...
}

```

#### Repository:
```php
<?php
// use the get() method (the only method on this repo), to fetch the only row
$entity = $repo->get();
```


<br/><br/><br/>


## Post Flush Subscriber Abstraction

The **AbstractPostFlushSubscriber** abstraction class will help to hook on the doctrine flush event.

```php
<?php

namespace App\EventListener;

use App\Entity\MyEntity;
use App\Entity\OtherEntity;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Events;
use GetRepo\DoctrineExtension\Listener\AbstractPostFlushListener;

#[AsDoctrineListener(event: Events::postPersist)]
#[AsDoctrineListener(event: Events::postUpdate)]
#[AsDoctrineListener(event: Events::postRemove)]
#[AsDoctrineListener(event: Events::postFlush)]
class PostFlushSubscriber extends AbstractPostFlushListener
{
    protected function getListenedClasses(): array
    {
        return [
            MyEntity::class => [Events::postUpdate, Events::postRemove],
            OtherEntity::class => [Events::postPersist],
        ];
    }

    protected function doPostFlush(object $entity, string $originEventName, EntityManagerInterface $em): void
    {
        // ... do stuffs
    }
}
```



<br/><br/><br/>


## Auto Validator

The **ValidatorListener** automatically validate doctrine entities on the `prePersist` and `preUpdate` events.


