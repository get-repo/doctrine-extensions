<?php

namespace GetRepo\DoctrineExtension\Settingsable;

use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Doctrine\ORM\Event\PrePersistEventArgs;
use Doctrine\ORM\Events;
use GetRepo\DoctrineExtension\Settingsable\Service\SettingsableServiceHandler;

#[AsDoctrineListener(event: Events::prePersist)]
class SettingsableListener
{
    public function __construct(
        private SettingsableServiceHandler $settingsableServiceHandler,
    ) {
    }

    public function prePersist(PrePersistEventArgs $eventArgs): void
    {
        $object = $eventArgs->getObject();
        $rClass = new \ReflectionClass($object);
        foreach ($rClass->getProperties() as $rProperty) {
            if ($settingsable = $this->settingsableServiceHandler->getAttribute($rProperty)) {
                ($this->settingsableServiceHandler)(
                    attribute: $settingsable,
                    object: $object,
                    target: $rProperty->getName(),
                );
            }
        }
    }
}
