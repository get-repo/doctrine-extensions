<?php

namespace GetRepo\DoctrineExtension\Settingsable\Service;

use Gedmo\Mapping\AttributeInterface;
use Gedmo\Mapping\Settingsable;
use GetRepo\DoctrineExtension\Cache\DoctrineExtensionCache;
use GetRepo\DoctrineExtension\Configuration\SettingsableConfiguration;
use GetRepo\DoctrineExtension\Exception\SettingsableException;
use GetRepo\DoctrineExtension\ServiceHandlerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyAccess\PropertyAccess;

class SettingsableServiceHandler implements ServiceHandlerInterface
{
    public function __construct(
        private DoctrineExtensionCache $cache,
    ) {
    }

    /** @param \ReflectionProperty $reflection */
    public static function getCacheKey(\Reflector $reflection): string
    {
        return sprintf('settingsable.%s.%s', $reflection->getDeclaringClass()->getName(), $reflection->getName());
    }

    /**
     * @param \ReflectionProperty $reflection
     * @return ?Settingsable
     */
    public function getAttribute(\Reflector $reflection): ?AttributeInterface
    {
        $cacheKey = self::getCacheKey($reflection);
        if ($this->cache->has($cacheKey)) {
            return $this->cache->get($cacheKey);
        }

        $settingsable = null;
        if ($settingsableAttributes = $reflection->getAttributes(Settingsable::class)) {
            if (count($settingsableAttributes) > 1) {
                throw new SettingsableException(sprintf(
                    'Field %s::$%s must have only 1 #[Settingsable] attribute, found %s',
                    $reflection->getDeclaringClass()->getName(),
                    $reflection->getName(),
                    count($settingsableAttributes),
                ));
            }
            /** @var Settingsable $settingsable */
            $settingsable = $settingsableAttributes[0]->newInstance();
        }

        $this->cache->set($cacheKey, $settingsable);

        return $settingsable;
    }

    /**
     * @param Settingsable $attribute
     */
    public function __invoke(AttributeInterface $attribute, object $object, string $target): void
    {
        $callback = [$className = get_class($object), $method = $attribute->getDefinitionMethod()];
        if (!is_callable($callback)) {
            throw new SettingsableException(sprintf(
                'Settingsable static method %s::%s() was not found',
                $className,
                $method,
            ));
        }
        $closure = \Closure::fromCallable([$object, $attribute->getDefinitionMethod()]); // @phpstan-ignore-line
        $definitions = SettingsableConfiguration::validate($closure());
        $resolver = new OptionsResolver();
        foreach ($definitions as $name => $conf) {
            if ($conf['mandatory']) {
                $resolver->setRequired($name);
            } else {
                $resolver->setDefault($name, $conf['default']);
            }
            if ($conf['allowed_types']) {
                $resolver->addAllowedTypes($name, $conf['allowed_types']);
            }
            if ($conf['allowed_values']) {
                $resolver->addAllowedValues($name, $conf['allowed_values']);
            }
        }

        $propertyAccessor = PropertyAccess::createPropertyAccessorBuilder()
            ->enableExceptionOnInvalidIndex()
            ->enableExceptionOnInvalidPropertyPath()
            ->getPropertyAccessor();

        try {
            $propertyAccessor->setValue(
                $object,
                $target,
                $resolver->resolve((array) $propertyAccessor->getValue($object, $target)),
            );
        } catch (\Exception $e) {
            $message = \sprintf(
                "Settingsable exception '%s':\n%s\nEntity %s",
                $e::class,
                $e->getMessage(),
                get_class($object),
            );

            throw new SettingsableException(message: $message, previous: $e);
        }
    }
}
