<?php

namespace GetRepo\DoctrineExtension\Settingsable\Service;

class SettingsableResolver
{
    private const DATETIME_KEY = '__datetime_class_name_3136ce7fe579729d844e4a99b7ed31ab__';

    public static function resolveGetValue(mixed $value): mixed
    {
        if (is_array($value) && isset($value[self::DATETIME_KEY])) {
            $value = new $value[self::DATETIME_KEY]($value['date'], new \DateTimeZone($value['timezone']));
        }

        return $value;
    }

    public static function resolveSetValue(mixed $value): mixed
    {
        if ($value instanceof \DateTimeInterface) {
            $value = array_merge([self::DATETIME_KEY => get_class($value)], (array)$value);
        }

        return $value;
    }
}
