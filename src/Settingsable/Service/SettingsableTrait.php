<?php

namespace GetRepo\DoctrineExtension\Settingsable\Service;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Settingsable;

trait SettingsableTrait
{
    #[Settingsable]
    #[ORM\Column(type: 'json')]
    protected array $settings = [];

    public function setSettings(array $settings): self
    {
        foreach ($settings as $name => $value) {
            $this->setSetting($name, $value);
        }

        return $this;
    }

    public function setSetting(string $name, mixed $value): self
    {
        $this->settings[$name] = SettingsableResolver::resolveSetValue($value);

        return $this;
    }

    public function getSettings(): array
    {
        $settings = [];
        foreach (array_keys($this->settings) as $name) {
            $settings[$name] = $this->getSetting($name);
        }

        return $settings;
    }

    public function getSetting(string $name): mixed
    {
        if ($this->hasSetting($name)) {
            return SettingsableResolver::resolveGetValue($this->settings[$name]);
        }

        throw new \InvalidArgumentException("Setting '{$name}' does not exist.");
    }

    public function hasSetting(string $name): bool
    {
        return array_key_exists($name, $this->settings);
    }
}
