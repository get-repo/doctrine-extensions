<?php

namespace GetRepo\DoctrineExtension;

use GetRepo\DoctrineExtension\DependencyInjection\GetRepoDoctrineExtension;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class GetRepoDoctrineExtensionBundle extends Bundle
{
    public function getContainerExtension(): ?ExtensionInterface
    {
        if (null === $this->extension) {
            $this->extension = $this->createContainerExtension();
        }

        return $this->extension;
    }

    protected function getContainerExtensionClass(): string
    {
        return GetRepoDoctrineExtension::class;
    }
}
