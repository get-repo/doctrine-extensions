<?php

namespace GetRepo\DoctrineExtension\Cache;

use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Contracts\Cache\CacheInterface;

class DoctrineExtensionCache
{
    public function __construct(
        /** @var \Symfony\Component\Cache\Adapter\AbstractAdapter $cache */
        #[Autowire(service: 'getrepo_doctrine_extension.cache')]
        private CacheInterface $cache,
    ) {
    }

    public function has(string $key): mixed
    {
        return $this->cache->getItem($key)->isHit();
    }

    public function set(string $key, mixed $value): void
    {
        $item = $this->cache->getItem($key);
        $item->set($value);
        $this->cache->save($item);
    }

    public function get(string $key): mixed
    {
        return $this->has($key) ? $this->cache->getItem($key)->get() : null;
    }

    public function delete(string $key): bool
    {
        return $this->cache->deleteItem($key);
    }
}
