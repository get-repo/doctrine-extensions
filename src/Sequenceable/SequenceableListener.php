<?php

namespace GetRepo\DoctrineExtension\Sequenceable;

use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Doctrine\ORM\Event\PrePersistEventArgs;
use Doctrine\ORM\Events;
use GetRepo\DoctrineExtension\Sequenceable\Service\SequenceableServiceHandler;

#[AsDoctrineListener(event: Events::prePersist)]
class SequenceableListener
{
    public function __construct(
        private SequenceableServiceHandler $sequenceableServiceHandler
    ) {
    }

    public function prePersist(PrePersistEventArgs $eventArgs): void
    {
        $object = $eventArgs->getObject();
        $rClass = new \ReflectionClass($object);
        foreach ($rClass->getProperties() as $rProperty) {
            if ($sequenceable = $this->sequenceableServiceHandler->getAttribute($rProperty)) {
                ($this->sequenceableServiceHandler)(
                    attribute: $sequenceable,
                    object: $object,
                    target: $rProperty->getName(),
                );
            }
        }
    }
}
