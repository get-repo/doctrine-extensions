<?php

namespace GetRepo\DoctrineExtension\Sequenceable\Service;

use Doctrine\ORM\EntityManagerInterface;
use Gedmo\Mapping\AttributeInterface;
use Gedmo\Mapping\Sequenceable;
use GetRepo\DoctrineExtension\Cache\DoctrineExtensionCache;
use GetRepo\DoctrineExtension\DependencyInjection\GetRepoDoctrineExtension;
use GetRepo\DoctrineExtension\Exception\SequenceableException;
use GetRepo\DoctrineExtension\ServiceHandlerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\DependencyInjection\Attribute\TaggedLocator;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\String\CodePointString;

class SequenceableServiceHandler implements ServiceHandlerInterface
{
    /** @var array{'enabled': bool, 'format_iterator': string} */
    protected array $config;

    /** @param array<mixed> $config */
    public function __construct(
        #[TaggedLocator(tag: GetRepoDoctrineExtension::ALIAS . '.sequenceable_service')]
        private ContainerInterface $handlers,
        private EntityManagerInterface $em,
        private DoctrineExtensionCache $cache,
        #[Autowire(param: GetRepoDoctrineExtension::ALIAS . '.config')]
        array $config,
    ) {
        $this->config = $config['sequenceable'];
    }

    /** @param \ReflectionProperty $reflection */
    public static function getCacheKey(\Reflector $reflection): string
    {
        return sprintf('sequenceable.%s.%s', $reflection->getDeclaringClass()->getName(), $reflection->getName());
    }

    /**
     * @param \ReflectionProperty $reflection
     * @return ?Sequenceable
     */
    public function getAttribute(\Reflector $reflection): ?AttributeInterface
    {
        $cacheKey = self::getCacheKey($reflection);
        if ($this->cache->has($cacheKey)) {
            return $this->cache->get($cacheKey);
        }

        $sequenceable = null;
        if ($sequenceableAttributes = $reflection->getAttributes(Sequenceable::class)) {
            if (count($sequenceableAttributes) > 1) {
                throw new SequenceableException(sprintf(
                    'Field %s::$%s must have only 1 #[Sequenceable] attribute, found %s',
                    $reflection->getDeclaringClass()->getName(),
                    $reflection->getName(),
                    count($sequenceableAttributes),
                ));
            }
            /** @var Sequenceable $sequenceable */
            $sequenceable = $sequenceableAttributes[0]->newInstance();
        }

        $this->cache->set($cacheKey, $sequenceable);

        return $sequenceable;
    }

    /**
     * @param Sequenceable $attribute
     */
    public function __invoke(AttributeInterface $attribute, object $object, string $target): void
    {
        $objectClass = get_class($object);

        // check service exists
        $serviceClass = $attribute->getService();
        if (!$this->handlers->has($serviceClass)) {
            throw new SequenceableException(sprintf(
                "Sequenceable service %s was not found.\nDid you implement the %s ?",
                $serviceClass,
                SequenceableServiceInterface::class,
            ));
        }

        // check format
        if ($format = $attribute->getFormat()) {
            if ($attribute->getFormatWithDate()) {
                $format = date($format);
            }
            $format = new CodePointString($format);
            if (!$format->containsAny($this->config['format_iterator'])) {
                throw new SequenceableException(sprintf(
                    "Sequenceable format is invalid in %s::$%s.\n" .
                    'Maybe the format is missing the iterator %s ?',
                    $objectClass,
                    $target,
                    $this->config['format_iterator'],
                ));
            }
        }

        // check grouping exists
        if ($groupings = $attribute->getGrouping()) {
            /** @var \Doctrine\ORM\Mapping\ClassMetadata $metadata */
            $metadata = $this->em->getClassMetadata($objectClass);
            foreach ($groupings as $grouping) {
                if (!$metadata->hasField($grouping) && !$metadata->hasAssociation($grouping)) {
                    throw new SequenceableException(sprintf(
                        'Sequenceable grouping field or association %s::$%s was not found.',
                        $objectClass,
                        $grouping,
                    ));
                }
            }
        }

        /** @var SequenceableServiceInterface $handler */
        $handler = $this->handlers->get($serviceClass);

        PropertyAccess::createPropertyAccessorBuilder()
            ->enableExceptionOnInvalidIndex()
            ->enableExceptionOnInvalidPropertyPath()
            ->getPropertyAccessor()
            ->setValue($object, $target, $handler->next($attribute, $object, $target));
    }
}
