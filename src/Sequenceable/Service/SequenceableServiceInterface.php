<?php

namespace GetRepo\DoctrineExtension\Sequenceable\Service;

use Gedmo\Mapping\Sequenceable;

interface SequenceableServiceInterface
{
    public function next(Sequenceable $attribute, object $object, string $field): string;
}
