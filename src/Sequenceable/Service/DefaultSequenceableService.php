<?php

namespace GetRepo\DoctrineExtension\Sequenceable\Service;

use Doctrine\ORM\EntityManagerInterface;
use Gedmo\Mapping\Sequenceable;
use GetRepo\DoctrineExtension\DependencyInjection\GetRepoDoctrineExtension;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\String\CodePointString;

class DefaultSequenceableService implements SequenceableServiceInterface
{
    /** @var array{'enabled': bool, 'format_iterator': string} */
    protected array $config;

    /** @param array<mixed> $config */
    public function __construct(
        private EntityManagerInterface $em,
        #[Autowire(param: GetRepoDoctrineExtension::ALIAS . '.config')]
        array $config,
    ) {
        $this->config = $config['sequenceable'];
    }

    public function next(Sequenceable $attribute, object $object, string $field): string
    {
        $objectClass = get_class($object);
        $pa = PropertyAccess::createPropertyAccessorBuilder()
            ->enableExceptionOnInvalidIndex()
            ->enableExceptionOnInvalidPropertyPath()
            ->getPropertyAccessor();

        $alias = 't';
        /** @var \Doctrine\ORM\EntityRepository $repo */
        $repo = $this->em->getRepository($objectClass);
        $qb = $repo->createQueryBuilder($alias);

        // sequence field should not be empty
        $qb->andWhere("{$alias}.{$field} IS NOT NULL");

        // add grouping conditions
        if ($groupings = $attribute->getGrouping()) {
            foreach ($groupings as $grouping) {
                $qb->andWhere("{$alias}.{$grouping} = :{$grouping}");
                $qb->setParameter($grouping, $pa->getValue($object, $grouping));
            }
        }

        // format
        $format = $attribute->getFormat();
        if ($format) {
            if ($attribute->getFormatWithDate()) {
                $format = date($format);
            }
            $qb->andWhere($qb->expr()->like("{$alias}.{$field}", ':format'));
            $qb->setParameter('format', \str_replace($this->config['format_iterator'], '%', $format));

            // perform a natural sorting
            $qb->orderBy("LENGTH({$alias}.{$field})", 'DESC');
        }

        $qb->addOrderBy("{$alias}.{$field}", 'DESC');
        $qb->setMaxResults(1);

        // find last element in the sequence
        $res = $qb->getQuery()->getOneOrNullResult();

        // get last sequence
        $newValue = $attribute->getStartAt();
        if ($res) {
            // last value found
            $last = $pa->getValue($res, $field);
            if ($format) {
                $formatString = new CodePointString($format);
                // create regexp with tmp replace hacky stuff
                $tmp = \str_repeat('TMP', 50);
                $regexp = $formatString->replace($this->config['format_iterator'], $tmp);
                $regexp = new CodePointString(sprintf('#%s#', \preg_quote($regexp)));
                $regexp = $regexp->replace($tmp, '([0-9]+)');
                $last = new CodePointString($last);
                if ($matches = $last->match($regexp)) {
                    $formatString = new CodePointString($format);
                    $newValue = $formatString->replace(
                        $this->config['format_iterator'],
                        (string) ((int) $matches[1] + $attribute->getStep()),
                    );
                }
            } else {
                $newValue = $last + $attribute->getStep();
            }
        } elseif ($format) {
            $formatString = new CodePointString($format);
            $newValue = $formatString->replace($this->config['format_iterator'], (string) $newValue);
        }

        return (string) $newValue;
    }
}
