<?php

namespace GetRepo\DoctrineExtension\Configuration;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Processor;

class SettingsableConfiguration implements ConfigurationInterface
{
    final public const ALIAS = 'settingsable';

    public static function validate(array $config): array
    {
        $processor = new Processor();
        $configuration = new self();

        return $processor->processConfiguration(
            $configuration,
            [$config]
        );
    }

    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder(self::ALIAS);
        $rootNode = $treeBuilder->getRootNode();

        // @phpstan-ignore-next-line
        $rootNode
            ->beforeNormalization()
                ->ifTrue(fn ($config) => is_array($config)
                    && \array_key_exists('mandatory', $config)
                    && \array_key_exists('default', $config)
                    && true === $config['mandatory'])
                ->thenInvalid('Settingsable "mandatory"=true with "default" is not possible')
            ->end()
            ->useAttributeAsKey('name')
            ->arrayPrototype()
                ->children()
                    ->booleanNode('mandatory')
                        ->defaultFalse()
                    ->end()
                    ->variableNode('default')
                        ->defaultNull()
                    ->end()
                    ->arrayNode('allowed_types')
                        ->scalarPrototype()->end()
                    ->end()
                    ->arrayNode('allowed_values')
                        ->scalarPrototype()->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
