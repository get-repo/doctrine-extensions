<?php

namespace GetRepo\DoctrineExtension\DependencyInjection;

use GetRepo\DoctrineExtension\Ownable\Filter\OwnableFilter;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class GetRepoDoctrineExtension extends Extension implements PrependExtensionInterface
{
    final public const ALIAS = 'getrepo_doctrine_extension';

    public function getAlias(): string
    {
        return self::ALIAS;
    }

    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        $container->setParameter(sprintf('%s.config', self::ALIAS), $config);

        $cacheDefinition = new Definition(
            $config['cache']['class'],
            ['$directory' => '%kernel.cache_dir%/' . self::ALIAS],
        );
        $container->setDefinition(sprintf('%s.cache', self::ALIAS), $cacheDefinition);

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../../config'));
        $loader->load("services.yml");
    }


    public function prepend(ContainerBuilder $container): void
    {
        $container->prependExtensionConfig('doctrine', [
            'orm' => [
                'entity_managers' => [
                    'default' => [
                        'filters' => [
                            'ownable' => [
                                'class' => OwnableFilter::class,
                                'enabled' => true,
                            ],
                        ],
                    ],
                ],
            ],
        ]);
    }
}
