<?php

namespace GetRepo\DoctrineExtension\DependencyInjection;

use GetRepo\DoctrineExtension\Origin\Service\OriginServiceHandler;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder(GetRepoDoctrineExtension::ALIAS);
        /** @var \Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition $rootNode */
        $rootNode = $treeBuilder->getRootNode();

        $rootNode // @phpstan-ignore-line
            ->children()
                ->arrayNode('cache')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('class')
                            ->cannotBeEmpty()
                            ->defaultValue(FilesystemAdapter::class)
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('sequenceable')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('format_iterator')
                            ->cannotBeEmpty()
                            ->defaultValue('{!}') // no letters to avoid date format issues
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('onerow')->end()
                ->arrayNode('origin')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('format')
                            ->defaultValue('{context}@{value}')
                            ->cannotBeEmpty()
                            ->example('{context}@{value}')
                            ->info('You can use placeholders {context} and {value}')
                        ->end()
                        ->arrayNode('rules')
                            ->addDefaultsIfNotSet()
                            ->children()
                                ->arrayNode(OriginServiceHandler::ORIGIN_CLI)
                                    ->scalarPrototype()->end()
                                ->end()
                                ->arrayNode(OriginServiceHandler::ORIGIN_WEB)
                                    ->scalarPrototype()->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
