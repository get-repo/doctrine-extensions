<?php

namespace GetRepo\DoctrineExtension\Listener;

use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/* Automatically validate doctrine entities on the prePersist and preUpdate events. */
#[AsDoctrineListener(event: Events::prePersist, priority: -256)]
#[AsDoctrineListener(event: Events::preUpdate, priority: -256)]
class ValidatorListener
{
    public function __construct(
        private readonly ValidatorInterface $validator,
    ) {
    }

    public function prePersist(LifecycleEventArgs $args): void
    {
        $this->validate($args);
    }

    public function preUpdate(LifecycleEventArgs $args): void
    {
        $this->validate($args);
    }

    private function validate(LifecycleEventArgs $args): void
    {
        $object = $args->getObject();
        $violations = $this->validator->validate($object);
        if ($violations->count()) {
            throw new ValidationFailedException($object, $violations);
        }
    }
}
