<?php

namespace GetRepo\DoctrineExtension\Listener;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Event\PostPersistEventArgs;
use Doctrine\ORM\Event\PostRemoveEventArgs;
use Doctrine\ORM\Event\PostUpdateEventArgs;
use Doctrine\ORM\Events;

/**
 * This abstract Doctrine post flush event listener is designed to be extended for handling postFlush events.
 * Unlike typical lifecycle events, postFlush does not provide the entity who triggered the event.
 * To address this limitation, this abstract class employs tracking mechanisms using postPersist, postUpdate events.
 *
 * Usage:
 * - Extend this abstract class in your custom postFlush event listener.
 * - Implement the required methods to define the logic for handling postFlush events.
 *
 * Note:
 * - While using this approach, be cautious with the logic implemented in the extended listener to ensure efficiency.
 * - This abstract class is particularly beneficial when handling postFlush events across multiple entity types.
 */
abstract class AbstractPostFlushListener
{
    /** @var array<string, true> */
    private static array $disabled = [];
    /** @var array<string, array<string, array<mixed>>> */
    private static array $map = [];

    abstract protected function getListenedClasses(): array;

    abstract protected function doPostFlush(object $entity, string $originEventName, EntityManagerInterface $em): void;

    public static function enable(): void
    {
        unset(self::$disabled[static::class]);
    }

    public static function disable(): void
    {
        self::$disabled[static::class] = true;
    }

    public static function isEnabled(): bool
    {
        return !isset(self::$disabled[static::class]);
    }

    public function postPersist(PostPersistEventArgs $eventArgs): void
    {
        $this->listen($eventArgs, Events::postPersist);
    }

    public function postUpdate(PostUpdateEventArgs $eventArgs): void
    {
        $this->listen($eventArgs, Events::postUpdate);
    }

    public function postRemove(PostRemoveEventArgs $eventArgs): void
    {
        $this->listen($eventArgs, Events::postRemove);
    }

    public function postFlush(PostFlushEventArgs $eventArgs): void
    {
        $wasEnabled = self::isEnabled();
        if ($wasEnabled) { // disable to avoid flush loops for same listener
            self::disable();
        }
        $map = self::$map[static::class] ?? [];
        unset(self::$map[static::class]);
        /** @var EntityManagerInterface $em */
        $em = $eventArgs->getObjectManager();
        foreach ($map as $originEventName => $events) {
            foreach ($events as $entity) {
                $this->doPostFlush($entity, $originEventName, $em);
            }
        }
        if ($wasEnabled) { // re-enable listener if it was before
            self::enable();
        }
    }

    private function listen(LifecycleEventArgs $eventArgs, string $originEventName): void
    {
        if (self::isEnabled()) {
            $entity = $eventArgs->getObject();
            if (array_key_exists($entity::class, $this->getListenedClasses())) {
                $listenedEvents = $this->getListenedClasses()[$entity::class];
                if (is_null($listenedEvents)) { // if null, then we listen to all events
                    $listenedEvents = [Events::postPersist, Events::postUpdate, Events::postRemove];
                }
                if (in_array($originEventName, (array) $listenedEvents)) {
                    self::$map[static::class][$originEventName][] = $entity;
                }
            }
        }
    }
}
