<?php

namespace GetRepo\DoctrineExtension;

use Gedmo\Mapping\AttributeInterface;

interface ServiceHandlerInterface
{
    public static function getCacheKey(\Reflector $reflection): string;
    public function getAttribute(\Reflector $reflection): ?AttributeInterface;
    public function __invoke(AttributeInterface $attribute, object $object, string $target): void;
}
