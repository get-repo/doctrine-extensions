<?php

namespace GetRepo\DoctrineExtension\Ownable\Filter;

use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;
use GetRepo\DoctrineExtension\Ownable\Service\OwnableServiceHandler;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

class OwnableFilter extends SQLFilter
{
    private static Security $security;
    private static OwnableServiceHandler $ownableServiceHandler;
    private static PropertyAccessorInterface $pa;

    public static function setDependencies(Security $security, OwnableServiceHandler $ownableServiceHandler): void
    {
        self::$security = $security;
        self::$ownableServiceHandler = $ownableServiceHandler;
        self::$pa = PropertyAccess::createPropertyAccessorBuilder()
            ->enableExceptionOnInvalidIndex()
            ->enableExceptionOnInvalidPropertyPath()
            ->getPropertyAccessor();
    }

    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias): string
    {
        if (!isset(self::$security) || !isset(self::$ownableServiceHandler)) {
            return '';
        }
        $parts = [];
        $user = self::$security->getUser();
        foreach ($targetEntity->getAssociationMappings() as $name => $associationMapping) {
            $rProperty = new \ReflectionProperty($targetEntity->getName(), $name);
            if ($ownable = self::$ownableServiceHandler->getAttribute($rProperty)) {
                if (self::$security->isGranted($ownable->getRoleAdmin())) {
                    continue;
                }

                $platform = $this->getConnection()->getDatabasePlatform();
                $columnName = $associationMapping['joinColumns'][0]['name'] ?? $name;
                $column = sprintf(
                    '%s.%s',
                    $targetTableAlias,
                    $platform->quoteIdentifier($columnName),
                );

                if ($user) {
                    $identifier = $associationMapping['sourceToTargetKeyColumns'][$columnName] ?? 'id';
                    $id = self::$pa->getValue($user, $identifier);
                    $parts[] = sprintf('%s = %s', $column, $platform->quoteStringLiteral($id));
                } else {
                    $parts[] = sprintf('%s IS NULL', $column);
                }
            }
        }

        return implode(' AND ', $parts);
    }
}
