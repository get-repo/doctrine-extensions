<?php

namespace GetRepo\DoctrineExtension\Ownable;

use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Doctrine\ORM\Event\PrePersistEventArgs;
use Doctrine\ORM\Events;
use GetRepo\DoctrineExtension\Ownable\Service\OwnableServiceHandler;

#[AsDoctrineListener(event: Events::prePersist)]
class OwnableListener
{
    public function __construct(
        private OwnableServiceHandler $ownableServiceHandler,
    ) {
    }

    public function prePersist(PrePersistEventArgs $eventArgs): void
    {
        $object = $eventArgs->getObject();
        $rClass = new \ReflectionClass($object);
        foreach ($rClass->getProperties() as $rProperty) {
            if ($ownable = $this->ownableServiceHandler->getAttribute($rProperty)) {
                ($this->ownableServiceHandler)(
                    attribute: $ownable,
                    object: $object,
                    target: $rProperty->getName(),
                );
            }
        }
    }
}
