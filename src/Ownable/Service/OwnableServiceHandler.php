<?php

namespace GetRepo\DoctrineExtension\Ownable\Service;

use Gedmo\Mapping\AttributeInterface;
use Gedmo\Mapping\Ownable;
use GetRepo\DoctrineExtension\Cache\DoctrineExtensionCache;
use GetRepo\DoctrineExtension\Exception\OwnableException;
use GetRepo\DoctrineExtension\Ownable\Filter\OwnableFilter;
use GetRepo\DoctrineExtension\ServiceHandlerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\PropertyAccess\PropertyAccess;

class OwnableServiceHandler implements ServiceHandlerInterface
{
    public function __construct(
        private Security $security,
        private DoctrineExtensionCache $cache,
    ) {
        OwnableFilter::setDependencies(security: $security, ownableServiceHandler: $this);
    }

    /** @param \ReflectionProperty $reflection */
    public static function getCacheKey(\Reflector $reflection): string
    {
        return sprintf('ownable.%s.%s', $reflection->getDeclaringClass()->getName(), $reflection->getName());
    }

    /**
     * @param \ReflectionProperty $reflection
     * @return ?Ownable
     */
    public function getAttribute(\Reflector $reflection): ?AttributeInterface
    {
        $cacheKey = self::getCacheKey($reflection);
        if ($this->cache->has($cacheKey)) {
            return $this->cache->get($cacheKey);
        }

        $ownable = null;
        if ($ownableAttributes = $reflection->getAttributes(Ownable::class)) {
            if (count($ownableAttributes) > 1) {
                throw new OwnableException(sprintf(
                    'Field %s::$%s must have only 1 #[Ownable] attribute, found %s',
                    $reflection->getDeclaringClass()->getName(),
                    $reflection->getName(),
                    count($ownableAttributes),
                ));
            }
            /** @var Ownable $ownable */
            $ownable = $ownableAttributes[0]->newInstance();
        }

        $this->cache->set($cacheKey, $ownable);

        return $ownable;
    }

    /**
     * @param Ownable $attribute
     */
    public function __invoke(AttributeInterface $attribute, object $object, string $target): void
    {
        if ($user = $this->security->getUser()) {
            $propertyAccessor = PropertyAccess::createPropertyAccessorBuilder()
                ->enableExceptionOnInvalidIndex()
                ->enableExceptionOnInvalidPropertyPath()
                ->getPropertyAccessor();
            $propertyAccessor->setValue($object, $target, $user);
        }
    }
}
