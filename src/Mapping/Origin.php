<?php

namespace Gedmo\Mapping;

use Attribute;

#[Attribute(Attribute::TARGET_PROPERTY)]
class Origin implements AttributeInterface
{
}
