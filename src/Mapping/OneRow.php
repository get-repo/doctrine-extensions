<?php

namespace Gedmo\Mapping;

use Attribute;

#[Attribute(Attribute::TARGET_CLASS)]
class OneRow implements AttributeInterface
{
    public function __construct(
        private bool $instantiate = false,
    ) {
    }

    public function getInstantiate(): bool
    {
        return $this->instantiate;
    }
}
