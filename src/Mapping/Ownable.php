<?php

namespace Gedmo\Mapping;

use Attribute;

#[Attribute(Attribute::TARGET_PROPERTY)]
class Ownable implements AttributeInterface
{
    public const DEFAULT_ROLEADMIN = 'ROLE_ADMIN';

    public function __construct(
        private string $roleAdmin = self::DEFAULT_ROLEADMIN,
    ) {
    }

    public function getRoleAdmin(): string
    {
        return $this->roleAdmin;
    }
}
