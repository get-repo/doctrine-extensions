<?php

namespace Gedmo\Mapping;

use Attribute;

#[Attribute(Attribute::TARGET_PROPERTY)]
class Settingsable implements AttributeInterface
{
    public const DEFAULT_METHOD = 'getSettingDefinitions';

    public function __construct(
        private string $definitionMethod = self::DEFAULT_METHOD,
    ) {
    }

    public function getDefinitionMethod(): string
    {
        return $this->definitionMethod;
    }
}
