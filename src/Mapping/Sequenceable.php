<?php

namespace Gedmo\Mapping;

use Attribute;
use GetRepo\DoctrineExtension\Sequenceable\Service\DefaultSequenceableService;

#[Attribute(Attribute::TARGET_PROPERTY)]
class Sequenceable implements AttributeInterface
{
    public const DEFAULT_START_AT = 1;
    public const DEFAULT_STEP = 1;
    public const DEFAULT_GROUPING = null;
    public const DEFAULT_SERVICE = DefaultSequenceableService::class;

    public function __construct(
        private int $startAt = self::DEFAULT_START_AT,
        private int $step = self::DEFAULT_STEP,
        private string $service = self::DEFAULT_SERVICE,
        /** @var array<string> $grouping */
        private null|string|array $grouping = self::DEFAULT_GROUPING,
        private ?string $format = null,
        private bool $formatWithDate = false,
    ) {
    }

    public function getStartAt(): int
    {
        return $this->startAt;
    }

    public function getStep(): int
    {
        return $this->step;
    }

    public function getService(): string
    {
        return $this->service;
    }

    /** @return array<string> */
    public function getGrouping(): ?array
    {
        return $this->grouping ? (array) $this->grouping : null;
    }

    public function getFormat(): ?string
    {
        return $this->format;
    }

    public function getFormatWithDate(): bool
    {
        return $this->formatWithDate;
    }
}
