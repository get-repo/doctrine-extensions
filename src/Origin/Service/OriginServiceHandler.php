<?php

namespace GetRepo\DoctrineExtension\Origin\Service;

use Gedmo\Mapping\AttributeInterface;
use Gedmo\Mapping\Origin;
use GetRepo\DoctrineExtension\Cache\DoctrineExtensionCache;
use GetRepo\DoctrineExtension\DependencyInjection\GetRepoDoctrineExtension;
use GetRepo\DoctrineExtension\Exception\OriginException;
use GetRepo\DoctrineExtension\ServiceHandlerInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\ExpressionLanguage\SyntaxError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\String\CodePointString;

class OriginServiceHandler implements ServiceHandlerInterface
{
    public const ORIGIN_CLI = 'cli';
    public const ORIGIN_WEB = 'web';

    private ?Request $request = null;
    /** @var array{'format': string, 'rules': array<mixed>} */
    protected array $config;

    public function __construct(
        private DoctrineExtensionCache $cache,
        RequestStack $requestStack,
        #[Autowire(param: GetRepoDoctrineExtension::ALIAS . '.config')]
        array $config,
    ) {
        $this->request = $requestStack->getMainRequest();
        $this->config = $config['origin'];
    }

    /** @param \ReflectionProperty $reflection */
    public static function getCacheKey(\Reflector $reflection): string
    {
        return sprintf('origin.%s.%s', $reflection->getDeclaringClass()->getName(), $reflection->getName());
    }

    /**
     * @param \ReflectionProperty $reflection
     * @return ?Origin
     */
    public function getAttribute(\Reflector $reflection): ?AttributeInterface
    {
        $cacheKey = self::getCacheKey($reflection);
        if ($this->cache->has($cacheKey)) {
            return $this->cache->get($cacheKey);
        }

        $origin = null;
        if ($originAttributes = $reflection->getAttributes(Origin::class)) {
            if (count($originAttributes) > 1) {
                throw new OriginException(sprintf(
                    'Field %s::$%s must have only 1 #[Origin] attribute, found %s',
                    $reflection->getDeclaringClass()->getName(),
                    $reflection->getName(),
                    count($originAttributes),
                ));
            }
            /** @var Origin $origin */
            $origin = $originAttributes[0]->newInstance();
        }

        $this->cache->set($cacheKey, $origin);

        return $origin;
    }

    /**
     * @param Origin $attribute
     */
    public function __invoke(AttributeInterface $attribute, object $object, string $target): void
    {
        $origin = match (true) {
            self::ORIGIN_CLI === php_sapi_name() => $this->handleCli(),
            $this->request instanceof Request => $this->handleWeb(),
            default => null,
        };

        if ($origin) {
            PropertyAccess::createPropertyAccessorBuilder()
                ->enableExceptionOnInvalidIndex()
                ->enableExceptionOnInvalidPropertyPath()
                ->getPropertyAccessor()
                ->setValue($object, $target, $origin);
        }
    }

    private function handleCli(): string
    {
        $rules = $this->config['rules'][self::ORIGIN_CLI];
        $argv = $_SERVER['argv'] ?? [];

        // search for symfony command if exists, and try to find it
        $symfonyCommand = false;
        if (preg_grep("#bin/console#", $argv)) {
            // try to match blabl:blabla in symfony command
            $matches = preg_grep("#[^:]+:[^:]+#", $argv);
            if (1 === (is_countable($matches) ? \count($matches) : 0)) {
                /** @var array $matches */
                $symfonyCommand = current($matches);
            }
        }

        return $this->checkRules(
            self::ORIGIN_CLI,
            $rules,
            ['argv' => $argv, 'sf_command' => $symfonyCommand]
        );
    }

    private function handleWeb(): string
    {
        $rules = $this->config['rules'][self::ORIGIN_WEB];

        return $this->checkRules(
            self::ORIGIN_WEB,
            $rules,
            ['request' => $this->request]
        );
    }

    private function checkRules(string $context, array $rules, array $expressionValues): string
    {
        $el = new ExpressionLanguage();
        foreach ($rules as $name => $expression) {
            try {
                $value = $el->evaluate($expression, $expressionValues);
            } catch (SyntaxError $e) {
                // throw just a warning
                @trigger_error('Origin CLI rules syntax error: ' . $e->getMessage(), E_USER_WARNING);
                continue;
            }
            if ($value) {
                return (new CodePointString($this->config['format']))
                    ->replace('{context}', $context)
                    ->replace('{value}', (\is_string($value) ? $value : $name));
            }
        }

        return $context;
    }
}
