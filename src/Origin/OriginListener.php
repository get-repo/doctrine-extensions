<?php

namespace GetRepo\DoctrineExtension\Origin;

use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Doctrine\ORM\Event\PrePersistEventArgs;
use Doctrine\ORM\Events;
use GetRepo\DoctrineExtension\Origin\Service\OriginServiceHandler;

#[AsDoctrineListener(event: Events::prePersist)]
class OriginListener
{
    public function __construct(
        private OriginServiceHandler $originServiceHandler
    ) {
    }

    public function prePersist(PrePersistEventArgs $eventArgs): void
    {
        $object = $eventArgs->getObject();
        $rClass = new \ReflectionClass($object);
        foreach ($rClass->getProperties() as $rProperty) {
            if ($origin = $this->originServiceHandler->getAttribute($rProperty)) {
                ($this->originServiceHandler)(
                    attribute: $origin,
                    object: $object,
                    target: $rProperty->getName(),
                );
            }
        }
    }
}
