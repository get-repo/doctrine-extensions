<?php

namespace GetRepo\DoctrineExtension\Origin\Traits;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Origin;

trait OriginEntity
{
    #[Origin]
    #[ORM\Column(nullable: true)]
    private ?string $contextOrigin = null;

    public function getContextOrigin(): ?string
    {
        return $this->contextOrigin;
    }

    public function setContextOrigin(?string $contextOrigin): self
    {
        $this->contextOrigin = $contextOrigin;

        return $this;
    }
}
