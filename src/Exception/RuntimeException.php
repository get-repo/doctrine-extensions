<?php

namespace GetRepo\DoctrineExtension\Exception;

class RuntimeException extends DoctrineExtensionException
{
}
