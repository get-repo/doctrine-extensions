<?php

namespace GetRepo\DoctrineExtension\Exception;

class OwnableException extends RuntimeException
{
}
