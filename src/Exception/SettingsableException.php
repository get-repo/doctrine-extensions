<?php

namespace GetRepo\DoctrineExtension\Exception;

class SettingsableException extends DoctrineExtensionException
{
}
