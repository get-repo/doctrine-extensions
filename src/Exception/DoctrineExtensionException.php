<?php

namespace GetRepo\DoctrineExtension\Exception;

abstract class DoctrineExtensionException extends \RuntimeException
{
}
