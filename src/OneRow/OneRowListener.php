<?php

namespace GetRepo\DoctrineExtension\OneRow;

use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Event\PrePersistEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use GetRepo\DoctrineExtension\OneRow\Service\OneRowRepository;
use GetRepo\DoctrineExtension\OneRow\Service\OneRowServiceHandler;

#[AsDoctrineListener(event: Events::loadClassMetadata)]
#[AsDoctrineListener(event: Events::prePersist)]
#[AsDoctrineListener(event: Events::preUpdate)]
class OneRowListener
{
    public function __construct(
        private OneRowServiceHandler $oneRowServiceHandler,
    ) {
    }

    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs): void
    {
        /** @var \Doctrine\ORM\Mapping\ClassMetadataInfo $metadata */
        $metadata = $eventArgs->getClassMetadata();
        $oneRow = $this->oneRowServiceHandler->getAttribute($metadata->getReflectionClass());
        if ($oneRow) {
            // set repository (I am not sure this is the right way to do this ?)
            $metadata->customRepositoryClassName = OneRowRepository::class;
        }
    }

    public function prePersist(PrePersistEventArgs $eventArgs): void
    {
        $this->handlePreEvent($eventArgs);
    }

    public function preUpdate(PreUpdateEventArgs $eventArgs): void
    {
        $this->handlePreEvent($eventArgs);
    }

    private function handlePreEvent(LifecycleEventArgs $eventArgs): void
    {
        $object = $eventArgs->getObject();
        $className = get_class($object);

        $oneRow = $this->oneRowServiceHandler->getAttribute(new \ReflectionClass($className));
        if ($oneRow) {
            ($this->oneRowServiceHandler)(
                attribute: $oneRow,
                object: $object,
                target: $className,
            );
        }
    }
}
