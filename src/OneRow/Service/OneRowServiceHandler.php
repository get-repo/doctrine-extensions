<?php

namespace GetRepo\DoctrineExtension\OneRow\Service;

use Doctrine\ORM\EntityManagerInterface;
use Gedmo\Mapping\AttributeInterface;
use Gedmo\Mapping\OneRow;
use GetRepo\DoctrineExtension\Cache\DoctrineExtensionCache;
use GetRepo\DoctrineExtension\Exception\OneRowException;
use GetRepo\DoctrineExtension\ServiceHandlerInterface;

class OneRowServiceHandler implements ServiceHandlerInterface
{
    public function __construct(
        private EntityManagerInterface $em,
        private DoctrineExtensionCache $cache,
    ) {
    }

    /** @param \ReflectionClass $reflection */
    public static function getCacheKey(\Reflector $reflection): string
    {
        return sprintf('one_row.%s', $reflection->getName());
    }

    /**
     * @param \ReflectionClass $reflection
     * @return ?OneRow
     */
    public function getAttribute(\Reflector $reflection): ?AttributeInterface
    {
        $cacheKey = self::getCacheKey($reflection);
        if ($this->cache->has($cacheKey)) {
            return $this->cache->get($cacheKey);
        }

        $oneRow = null;
        $className = $reflection->getName();
        while ($className) {
            if ($oneRowAttributes = $reflection->getAttributes(OneRow::class)) {
                if (count($oneRowAttributes) > 1) {
                    throw new OneRowException(sprintf(
                        'Class %s must have only 1 #[OneRow] attribute, found %s',
                        $className,
                        count($oneRowAttributes),
                    ));
                }
                /** @var OneRow $oneRow */
                $oneRow = $oneRowAttributes[0]->newInstance();
                break;
            }
            $className = get_parent_class($className);
        }

        $this->cache->set($cacheKey, $oneRow);

        return $oneRow;
    }

    /**
     * @param OneRow $attribute
     */
    public function __invoke(AttributeInterface $attribute, object $object, string $target): void
    {
        OneRowRepository::setDoctrineExtensionCache($this->cache);
        /** @var OneRowRepository $repo */
        $repo = $this->em->getRepository($target); // @phpstan-ignore-line
        $res = $repo->createQueryBuilder('e')->getQuery()->getResult();

        if ($res) {
            if ((is_countable($res) ? count($res) : 0) > 1 || $object !== current($res)) {
                throw new OneRowException("Entity {$target} is limited to 1 row only.");
            }
        }
    }
}
