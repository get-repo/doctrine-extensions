<?php

namespace GetRepo\DoctrineExtension\OneRow\Service;

use Doctrine\ORM\EntityRepository;
use Gedmo\Mapping\OneRow;
use GetRepo\DoctrineExtension\Cache\DoctrineExtensionCache;
use GetRepo\DoctrineExtension\Exception\OneRowException;

final class OneRowRepository extends EntityRepository
{
    private static DoctrineExtensionCache $cache;

    public static function setDoctrineExtensionCache(DoctrineExtensionCache $cache): void
    {
        self::$cache = $cache;
    }

    public function get(): ?object
    {
        $value = $this->_em->getUnitOfWork()
            ->getEntityPersister($this->_entityName)
            ->load([], null, null, [], null, 1);

        if (!$value && isset(self::$cache)) {
            $rClass = new \ReflectionClass($this->_entityName);
            /** @var OneRow $oneRow */
            $oneRow = self::$cache->get(OneRowServiceHandler::getCacheKey($rClass));
            if ($oneRow->getInstantiate()) {
                $value = $rClass->newInstance();
            }
        }

        return $value;
    }

    public function find(...$args)
    {
        throw $this->buildException(__METHOD__);
    }

    public function findAll()
    {
        throw $this->buildException(__METHOD__);
    }

    public function findBy(...$args)
    {
        throw $this->buildException(__METHOD__);
    }

    public function findOneBy(...$args)
    {
        throw $this->buildException(__METHOD__);
    }

    private function buildException(string $method): OneRowException
    {
        $message = sprintf(
            'Not allowed to call the %s() method. Please use the get() method.',
            $method
        );

        return new OneRowException($message);
    }
}
