<?php

namespace Test;

use Doctrine\Persistence\ObjectManager;
use GetRepo\SqliteDoctrineTest\SqliteTestCase;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class DoctrineExtensionTestCase extends SqliteTestCase
{
    protected static ObjectManager $em;
    protected static ValidatorInterface $validator;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        self::$em = self::getDoctrine()->getManager();
        self::$validator = static::getContainer()->get(ValidatorInterface::class); // @phpstan-ignore-line
    }
}
