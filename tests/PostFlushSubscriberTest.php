<?php

namespace Test;

use Test\Entity\EventTestEntity;

class PostFlushSubscriberTest extends DoctrineExtensionTestCase
{
    public function testPostFlush(): void
    {
        $em = self::getDoctrine()->getManagerForClass(EventTestEntity::class);

        // test persist
        $entity = new EventTestEntity();
        $entity->setNumero(1); // lower than 5
        /** @var \Symfony\Component\Validator\ConstraintViolationList $errors */
        $errors = self::$validator->validate($entity);
        $this->assertCount(0, $errors, (string) $errors);
        $em->persist($entity);
        $em->flush();
        $em->refresh($entity);

        $this->assertEquals(false, $entity->hasChanged());

        // test update event
        $entity->setNumero(10); // greater than 5
        /** @var \Symfony\Component\Validator\ConstraintViolationList $errors */
        $errors = self::$validator->validate($entity);
        $this->assertCount(0, $errors, (string) $errors);
        $em->persist($entity);
        $em->flush();
        $em->refresh($entity);

        $this->assertEquals(true, $entity->hasChanged());

        // test update event again with lower than 5
        $entity->setNumero(4); // greater than 5
        /** @var \Symfony\Component\Validator\ConstraintViolationList $errors */
        $errors = self::$validator->validate($entity);
        $this->assertCount(0, $errors, (string) $errors);
        $em->persist($entity);
        $em->flush();
        $em->refresh($entity);

        $this->assertEquals(false, $entity->hasChanged());

        // test remove event
        $entity->setNumero(9999999);
        $em->persist($entity);
        $em->flush();
        $em->refresh($entity);
        $em->remove($entity);
        $em->flush();

        // test deleted entity with numero = test_delete
        $entity = $em->getRepository(EventTestEntity::class)->findOneBy(['numero' => 9999999]);
        $this->assertNull($entity);
        // test new entity with numero = deleted_ok
        $entity = $em->getRepository(EventTestEntity::class)->findOneBy(['numero' => 8888888]);
        $this->assertInstanceOf(EventTestEntity::class, $entity);

        // test remove event
        $em->remove($entity);
        $em->flush();
    }
}
