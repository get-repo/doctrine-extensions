<?php

namespace Test;

use Doctrine\ORM\EntityManagerInterface;
use GetRepo\DoctrineExtension\Exception\OneRowException;
use GetRepo\DoctrineExtension\OneRow\Service\OneRowRepository;
use Test\Entity\OneRowInstantiateTestEntity;
use Test\Entity\OneRowTestEntity;

class OneRowTest extends DoctrineExtensionTestCase
{
    public function tearDown(): void
    {
        parent::tearDown();
        // cleaning
        $em = $this->getEntityManager();
        $conn = $em->getConnection();
        foreach (['one_row', 'one_row_instantiate'] as $table) {
            $conn->executeStatement($conn->getDatabasePlatform()->getTruncateTableSql($table));
        }
    }

    public function testLimit(): void
    {
        $this->buildRow(OneRowTestEntity::class);
        $em = $this->getEntityManager();

        // we expect an exception for the third result
        $this->expectException(OneRowException::class);
        $this->expectExceptionMessage(\sprintf(
            'Entity %s is limited to 1 row only.',
            OneRowTestEntity::class
        ));

        // add another result, and fails
        $entity = new OneRowTestEntity();
        $entity->setValue(2);
        $em->persist($entity);
        $em->flush();


        // add another result, and fails
        $entity = new OneRowTestEntity();
        $entity->setValue(3);
        $em->persist($entity);
        $em->flush();
    }

    public function testRepositoryClass(): void
    {
        $this->assertEquals(
            OneRowRepository::class,
            get_class($this->getEntityManager()->getRepository(OneRowTestEntity::class))
        );
    }

    public function dataGetMethod(): array
    {
        return [
            'instantiate disabled' => [OneRowTestEntity::class, false],
            'instantiate enabled' => [OneRowInstantiateTestEntity::class, true],
        ];
    }

    /**
     * @dataProvider dataGetMethod
     */
    public function testRepositoryGetMethod(string $className, bool $expectedInstance = false): void
    {
        /** @var OneRowRepository $repository */
        $repository = $this->getEntityManager()->getRepository($className); // @phpstan-ignore-line
        /** @var OneRowTestEntity|OneRowInstantiateTestEntity|null $res */
        $res = $repository->get();
        if ($expectedInstance) {
            $this->assertInstanceOf($className, $res); // @phpstan-ignore-line
            $this->assertNull($res->getId());
        } else {
            $this->assertNull($res);
        }
        $this->buildRow($className);
        /** @var OneRowTestEntity|OneRowInstantiateTestEntity|null $res */
        $res = $repository->get();
        $this->assertInstanceOf($className, $res); // @phpstan-ignore-line
        $this->assertNotNull($res->getId());
        $this->assertEquals(1, $res->getValue());
    }

    public function dataNotAllowedMethods(): array
    {
        return [
            'find method' => ['find', 1],
            'findBy method' => ['findBy', []],
            'findOneBy method' => ['findOneBy', []],
            'findAll method' => ['findAll'],
        ];
    }

    /**
     * @dataProvider dataNotAllowedMethods
     * @param mixed $arg
     */
    public function testRepositoryFindMethod(string $method, mixed $arg = null): void
    {
        // we expect an exception for the third result
        $this->expectException(OneRowException::class);
        $this->expectExceptionMessage(\sprintf(
            'Not allowed to call the %s::%s() method. ' .
            'Please use the get() method.',
            OneRowRepository::class,
            $method
        ));
        $repo = $this->getEntityManager()->getRepository(OneRowTestEntity::class);
        $this->assertInstanceOf(OneRowRepository::class, $repo);
        \call_user_func([$repo, $method], $arg); // @phpstan-ignore-line
    }

    private function buildRow(string $className): void
    {
        $em = $this->getEntityManager();

        // add a first result, should be ok
        /** @var OneRowTestEntity|OneRowInstantiateTestEntity $entity */
        $entity = new $className();
        $entity->setValue(1);
        $em->persist($entity);
        $em->flush();
        $this->assertInstanceOf($className, $entity); // @phpstan-ignore-line
    }

    private function getEntityManager(): EntityManagerInterface
    {
        /** @var EntityManagerInterface $em */
        $em = self::getDoctrine()->getManagerForClass(OneRowTestEntity::class);

        return $em;
    }
}
