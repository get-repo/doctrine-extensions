<?php

namespace Test\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Sequenceable;

#[ORM\Table(name: 'sequenceable_bad_grouping')]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class SequenceableBadGroupingTestEntity
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    protected int $id;

    #[Sequenceable(grouping: 'grouping_does_not_exists')]
    #[ORM\Column]
    private int $step; // @phpstan-ignore-line
}
