<?php

namespace Test\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'events')]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class EventTestEntity
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int $id; // @phpstan-ignore-line

    #[ORM\Column]
    private int $numero;

    #[ORM\Column]
    private bool $changed = false;

    public function getId(): int
    {
        return $this->id;
    }

    public function setNumero(int $numero): self
    {
        $this->numero = $numero;

        return $this;
    }

    public function getNumero(): int
    {
        return $this->numero;
    }

    public function setChanged(bool $changed): self
    {
        $this->changed = $changed;

        return $this;
    }

    public function hasChanged(): bool
    {
        return (bool) $this->changed;
    }
}
