<?php

namespace Test\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\OneRow;

#[ORM\Table(name: 'one_row_instantiate')]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
#[OneRow(instantiate: true)]
class OneRowInstantiateTestEntity
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null; // nullable important to test instantiate option

    #[ORM\Column]
    private int $value;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setValue(int $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getValue(): int
    {
        return $this->value;
    }
}
