<?php

namespace Test\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Sequenceable;

#[ORM\Table(name: 'sequenceable_attribute')]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class SequenceableTestEntity
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    protected int $id;

    #[Sequenceable]
    #[ORM\Column(type: 'integer')]
    private int $step;

    #[Sequenceable(step: 6)]
    #[ORM\Column(type: 'integer')]
    private int $step6;

    #[Sequenceable(startAt: 3)]
    #[ORM\Column(type: 'integer')]
    private int $startAt3;

    #[Sequenceable(startAt: 100, step: 5)]
    #[ORM\Column(type: 'integer')]
    private int $startAt100Step5;

    #[Sequenceable(grouping: 'grouped')]
    #[ORM\Column(type: 'integer')]
    private int $grouping;

    #[Sequenceable(format: 'FR-{!}-Y')]
    #[ORM\Column]
    private string $formatted;

    #[Sequenceable(format: '\C\O\O\L-Y-m-{\!}', formatWithDate: true)]
    #[ORM\Column]
    private string $formattedDate;

    public function __construct(
        #[ORM\Column(nullable: true)]
        private ?int $grouped = null,
    ) {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGrouped(): ?int
    {
        return $this->grouped;
    }

    public function getStep(): int
    {
        return $this->step;
    }

    public function setStep(int $step): self
    {
        $this->step = $step;

        return $this;
    }

    public function getStep6(): int
    {
        return $this->step6;
    }

    public function setStep6(int $step6): self
    {
        $this->step6 = $step6;

        return $this;
    }

    public function getStartAt3(): int
    {
        return $this->startAt3;
    }

    public function setStartAt3(int $startAt3): self
    {
        $this->startAt3 = $startAt3;

        return $this;
    }

    public function getStartAt100Step5(): int
    {
        return $this->startAt100Step5;
    }

    public function setStartAt100Step5(int $startAt100Step5): self
    {
        $this->startAt100Step5 = $startAt100Step5;

        return $this;
    }

    public function getGrouping(): int
    {
        return $this->grouping;
    }

    public function setGrouping(int $grouping): self
    {
        $this->grouping = $grouping;

        return $this;
    }

    public function getFormatted(): string
    {
        return $this->formatted;
    }

    public function setFormatted(string $formatted): self
    {
        $this->formatted = $formatted;

        return $this;
    }

    public function getFormattedDate(): string
    {
        return $this->formattedDate;
    }

    public function setFormattedDate(string $formattedDate): self
    {
        $this->formattedDate = $formattedDate;

        return $this;
    }
}
