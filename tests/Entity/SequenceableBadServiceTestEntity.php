<?php

namespace Test\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Sequenceable;

#[ORM\Table(name: 'sequenceable_bad_service')]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class SequenceableBadServiceTestEntity
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    protected int $id;

    #[Sequenceable(service: 'does_not_exists')]
    #[ORM\Column]
    private int $step; // @phpstan-ignore-line
}
