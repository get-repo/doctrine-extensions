<?php

namespace Test\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Sequenceable;

#[ORM\Table(name: 'sequenceable_bad_format')]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class SequenceableBadFormatTestEntity
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    protected int $id;

    #[Sequenceable(format: 'bad_format')]
    #[ORM\Column]
    private int $step; // @phpstan-ignore-line
}
