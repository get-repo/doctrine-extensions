<?php

namespace Test\Entity;

use Doctrine\ORM\Mapping as ORM;
use GetRepo\DoctrineExtension\Settingsable\Service\SettingsableTrait;

#[ORM\Table(name: 'settingsable')]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class SettingsableTestEntity
{
    use SettingsableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int $id; // @phpstan-ignore-line

    public static function getSettingDefinitions(): array
    {
        return [
            'no_conf' => [],
            'not_mandatory' => ['mandatory' => false],
            'mandatory' => ['mandatory' => true],
            'allowed_types' => ['default' => false, 'allowed_types' => ['boolean']],
            'allowed_values' => ['default' => 1, 'allowed_values' => [1, 2, 3]],
            'datetime' => ['mandatory' => false, 'allowed_types' => ['null', \DateTime::class]],
            'datetime_immutable' => ['mandatory' => false, 'allowed_types' => ['null', \DateTimeImmutable::class]],
        ];
    }

    public function getId(): int
    {
        return $this->id;
    }
}
