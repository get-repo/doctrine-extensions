<?php

namespace Test\Entity;

use Doctrine\ORM\Mapping as ORM;
use GetRepo\DoctrineExtension\Origin\Traits\OriginEntity;

#[ORM\Table(name: 'origin')]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class OriginTestEntity
{
    use OriginEntity;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int $id; // @phpstan-ignore-line

    public function getId(): int
    {
        return $this->id;
    }
}
