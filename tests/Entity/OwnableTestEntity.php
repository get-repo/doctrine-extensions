<?php

namespace Test\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Ownable;

#[ORM\Table(name: 'ownable')]
#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class OwnableTestEntity
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int $id; // @phpstan-ignore-line

    public function __construct(
        #[ORM\ManyToOne(targetEntity: UserEntity::class, cascade: ['persist'])]
        #[Ownable]
        private ?UserEntity $owner = null,
    ) {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setOwner(UserEntity $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getOwner(): ?UserEntity
    {
        return $this->owner;
    }
}
