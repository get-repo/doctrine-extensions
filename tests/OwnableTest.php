<?php

namespace Test;

use Symfony\Bundle\SecurityBundle\Security;
use Test\Entity\OwnableTestEntity;
use Test\Entity\UserEntity;

class OwnableTest extends DoctrineExtensionTestCase
{
    private static Security $security;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        self::$security = static::getContainer()->get(Security::class); // @phpstan-ignore-line
    }

    public function ownableData(): array
    {
        $em = self::getDoctrine()->getManager();

        // create new user
        $user = new UserEntity(name: 'user', roles: ['ROLE_USER']);
        $em->persist($user);
        $em->flush();

        // create new admin
        $admin = new UserEntity(name: 'admin', roles: ['ROLE_ADMIN']);
        $em->persist($admin);
        $em->flush();

        return [
            'not logged in' => [null, 2],
            'user' => [$user, 1],
            'admin' => [$admin, 3],
        ];
    }

    /**
     * @dataProvider ownableData
     */
    public function testOwnable(?UserEntity $loggedInUser, int $count): void
    {
        // truncate ownable table
        /** @var \Doctrine\DBAL\Connection $conn */
        $conn = self::$em->getConnection(); // @phpstan-ignore-line
        $truncateQuery = $conn->getDatabasePlatform()->getTruncateTableSql('ownable');
        $conn->executeStatement($truncateQuery);

        // create ownable row with no owner
        $row1 = new OwnableTestEntity();
        self::$em->persist($row1);
        self::$em->flush();
        $this->assertGreaterThan(0, $row1->getId());

        // create ownable row with another owner
        $owner = new UserEntity(name: 'another owner');
        self::$em->persist($owner);
        self::$em->flush();
        $row2 = new OwnableTestEntity(owner: $owner);
        self::$em->persist($row2);
        self::$em->flush();
        $this->assertGreaterThan(0, $row2->getId());

        // login user
        if ($loggedInUser) {
            self::$security->login($loggedInUser);
        }

        // check listener: create entity without owner
        $row3 = new OwnableTestEntity();
        self::$em->persist($row3);
        self::$em->flush();
        $this->assertGreaterThan(0, $row3->getId());

        $this->assertEquals($loggedInUser, $row3->getOwner());

        // check filter: get all OwnableTestEntity
        $all = self::$em->getRepository(OwnableTestEntity::class)->findAll();
        $message = '';
        foreach ($all as $row) {
            $message .= sprintf(
                ' %s,%s ',
                $row->getId(),
                ($row->getOwner() ? $row->getOwner()->getName() : 'NULL')
            );
        }
        $this->assertCount($count, $all, trim($message));
    }
}
