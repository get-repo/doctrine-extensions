<?php

namespace Test\EventListener;

use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Events;
use GetRepo\DoctrineExtension\Listener\AbstractPostFlushListener;
use Test\Entity\EventTestEntity;

#[AsDoctrineListener(event: Events::postPersist)]
#[AsDoctrineListener(event: Events::postUpdate)]
#[AsDoctrineListener(event: Events::postRemove)]
#[AsDoctrineListener(event: Events::postFlush)]
class PostFlushSubscriber extends AbstractPostFlushListener
{
    protected function getListenedClasses(): array
    {
        return [
            EventTestEntity::class => [Events::postUpdate, Events::postRemove],
        ];
    }

    protected function doPostFlush(object $entity, string $originEventName, EntityManagerInterface $em): void
    {
        /** @var EventTestEntity $entity */
        $uow = $em->getUnitOfWork();
        /** @var array $changeSet */
        $changeSet = $uow->getEntityChangeSet($entity);
        if (isset($changeSet['numero'][1])) {
            // if new numero value is > 5, we set changed value to TRUE, else FALSE
            $entity->setChanged(isset($changeSet['numero'][1]) && $changeSet['numero'][1] > 5);
            $em->persist($entity);
            $em->flush();
        }

        // logic to test the postRemove event
        if (Events::postRemove == $originEventName && 9999999 === $entity->getNumero()) {
            $entity = clone $entity;
            $entity->setNumero(8888888);
            $em->persist($entity);
            $em->flush();
        }
    }
}
