<?php

namespace Test;

use Symfony\Component\Validator\Exception\ValidationFailedException;
use Test\Entity\UserEntity;

class ValidatorListenerTest extends DoctrineExtensionTestCase
{
    public function testValidationExpection(): void
    {
        $this->expectException(ValidationFailedException::class);
        $this->expectExceptionMessage('Object(Test\Entity\UserEntity).name');
        $this->expectExceptionMessage('This value is too long');
        $entity = new UserEntity(name: str_repeat('too long', 300));
        self::$em->persist($entity);
        self::$em->flush();
    }
}
