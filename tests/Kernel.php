<?php

namespace Test;

use GetRepo\DoctrineExtension\GetRepoDoctrineExtensionBundle;
use GetRepo\SqliteDoctrineTest\AbstractKernel;
use Symfony\Bundle\SecurityBundle\SecurityBundle;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Test\Entity\UserEntity;

class Kernel extends AbstractKernel implements CompilerPassInterface
{
    /**
     * @return \Symfony\Component\HttpKernel\Bundle\Bundle[]
     */
    public function registeredBundles(): array
    {
        return [
            new SecurityBundle(),
            new GetRepoDoctrineExtensionBundle(),
        ];
    }

    public function registeredContainerConfiguration(ContainerBuilder $builder, LoaderInterface $loader): void
    {
        $builder->loadFromExtension('security', [
            'providers' => [
                'user_provider' => ['entity' => ['class' => UserEntity::class, 'property' => 'name']]
            ],
            'firewalls' => [
                'main' => [
                    'provider' => 'user_provider',
                    'http_basic' => null,
                ],
            ],
        ]);

        $builder->loadFromExtension('getrepo_doctrine_extension', [
            'origin' => [
                'format' => 'testing_{context}@{value}',
                'rules' => [ // NOTE argv has custom values in tests
                    'cli' => [
                        'phpunit' => 'argv[0] matches "/phpunit/"',
                        'custom' => 'argv[0] matches "/custom/" ? "custom_string" : false',
                        'symfony_command' => 'sf_command',
                    ],
                ]
            ],
        ]);

        $yamlLoader = new YamlFileLoader($builder, new FileLocator(__DIR__ . '/../config'));
        $yamlLoader->load("services_test.yml");
    }

    public function process(ContainerBuilder $container)
    {
        // create a fake request in stack for ownable test (mainly for $security->login($user))
        $definition = new Definition(Request::class); // fake request
        $container->setDefinition('test.request', $definition);
        $definition = new Definition(RequestStack::class); // fake request stack
        $definition->addMethodCall('push', [new Reference('test.request')]);
        $container->setDefinition('test.request_stack', $definition);
        $definition = $container->getDefinition('security.helper'); // replace request stack in security
        /** @var \Symfony\Component\DependencyInjection\Argument\ServiceLocatorArgument $serviceLocatorArgument */
        $serviceLocatorArgument = $definition->getArgument(0);
        $values = $serviceLocatorArgument->getValues();
        $values['request_stack'] = new Reference('test.request_stack');
        $serviceLocatorArgument->setValues($values);
        $definition->replaceArgument(0, $serviceLocatorArgument);
    }

    /**
     * @return array<string, mixed[]>
     */
    public function getDoctrineORMConfiguration(): array
    {
        return [
            'mappings' => [
                'src' => [
                    'type' => 'attribute',
                    'prefix' => 'Test\Entity',
                    'dir' => __DIR__ . '/Entity',
                ],
            ],
        ];
    }
}
