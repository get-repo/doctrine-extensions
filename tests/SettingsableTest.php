<?php

namespace Test;

use Test\Entity\SettingsableTestEntity;

class SettingsableTest extends DoctrineExtensionTestCase
{
    public function settingsableData(): array
    {
        return [
            // failures
            'mandatory missing' => [[], [], 'The required option "mandatory" is missing.'],
            'allowed_types error' => [
                [],
                ['mandatory' => 'ok', 'allowed_types' => 'should be boolean'],
                'is expected to be of type "boolean", but is of type "string".',
            ],
            'allowed_values error' => [
                [],
                ['mandatory' => 'ok', 'allowed_values' => 4],
                'The option "allowed_values" with value 4 is invalid. Accepted values are: 1, 2, 3.',
            ],
            // valid
            'just mandatory' => [
                [
                    'no_conf' => null,
                    'not_mandatory' => null,
                    'allowed_types' => false,
                    'allowed_values' => 1,
                    'mandatory' => 'ok',
                    'datetime' => null,
                    'datetime_immutable' => null,
                ],
                ['mandatory' => 'ok'],
            ],
            'allowed_types' => [
                [
                    'no_conf' => null,
                    'not_mandatory' => null,
                    'allowed_types' => true,
                    'allowed_values' => 1,
                    'mandatory' => 'ok',
                    'datetime' => null,
                    'datetime_immutable' => null,
                ],
                ['mandatory' => 'ok', 'allowed_types' => true],
            ],
            'allowed_values' => [
                [
                    'no_conf' => null,
                    'not_mandatory' => null,
                    'allowed_types' => false,
                    'allowed_values' => 3,
                    'mandatory' => 'ok',
                    'datetime' => null,
                    'datetime_immutable' => null,
                ],
                ['mandatory' => 'ok', 'allowed_values' => 3],
            ],
            'date resolver' => [
                [
                    'no_conf' => null,
                    'not_mandatory' => null,
                    'allowed_types' => false,
                    'allowed_values' => 1,
                    'mandatory' => 'test SettingsableResolver',
                    'datetime' => $date = new \DateTime('1111-11-11 Europe/Berlin'),
                    'datetime_immutable' => $datetime = new \DateTimeImmutable('2222-12-22', new \DateTimeZone('UTC')),
                ],
                [
                    'mandatory' => 'test SettingsableResolver',
                    'datetime' => $date,
                    'datetime_immutable' => $datetime,
                ],
            ],
        ];
    }

    /**
     * @dataProvider settingsableData
     */
    public function test(array $expected, array $settings, string $exception = null): void
    {
        if ($exception) {
            $this->expectExceptionMessage($exception);
        }

        $entity = new SettingsableTestEntity();
        $entity->setSettings($settings);
        self::$em->persist($entity);
        self::$em->flush();
        self::$em->refresh($entity);
        $this->assertEquals($expected, $entity->getSettings());
    }
}
