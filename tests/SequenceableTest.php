<?php

namespace Test;

use Test\Entity\SequenceableBadFormatTestEntity;
use Test\Entity\SequenceableBadGroupingTestEntity;
use Test\Entity\SequenceableBadServiceTestEntity;
use Test\Entity\SequenceableTestEntity;

class SequenceableTest extends DoctrineExtensionTestCase
{
    public function testSuccess(): void
    {
        for ($i = 1; $i <= 10; $i++) {
            $entity = new SequenceableTestEntity(grouped: (int) ceil($i / 3));
            self::$em->persist($entity);
            self::$em->flush();
            $this->assertEquals($i, $entity->getStep(), "step=1 #{$i}");
            $this->assertEquals(1 + (6 * ($i - 1)), $entity->getStep6(), "step=6 #{$i}");
            $this->assertEquals(3 + $i - 1, $entity->getStartAt3(), "start_at=3 #{$i}");
            $this->assertEquals(100 + (5 * ($i - 1)), $entity->getStartAt100Step5(), "start_at=100 step=5 #{$i}");
            $this->assertEquals($i % 3 ?: 3, $entity->getGrouping(), "grouping=['grouped'] #{$i}");
            $this->assertEquals(sprintf('FR-%d-Y', $i), $entity->getFormatted(), "formatted='FR-{i}-Y' #{$i}");
            $this->assertEquals(
                sprintf('COOL-%s-%d', date('Y-m'), $i),
                $entity->getFormattedDate(),
                "formatted_date='COOL-Y-m-{i}' #{$i}",
            );
        }
    }

    public function testServiceDoesNotExists(): void
    {
        $this->expectExceptionMessage('Sequenceable service does_not_exists was not found.');
        $entity = new SequenceableBadServiceTestEntity();
        self::$em->persist($entity);
        self::$em->flush();
    }

    public function testGroupingDoesNotExists(): void
    {
        $this->expectExceptionMessage(sprintf(
            'Sequenceable grouping field or association %s::$grouping_does_not_exists was not found.',
            SequenceableBadGroupingTestEntity::class
        ));
        $entity = new SequenceableBadGroupingTestEntity();
        self::$em->persist($entity);
        self::$em->flush();
    }

    public function testBadFormat(): void
    {
        $this->expectExceptionMessage(sprintf(
            'Sequenceable format is invalid in %s::$step.',
            SequenceableBadFormatTestEntity::class
        ));
        $entity = new SequenceableBadFormatTestEntity();
        self::$em->persist($entity);
        self::$em->flush();
    }
}
