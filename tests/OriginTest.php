<?php

namespace Test;

use Test\Entity\OriginTestEntity;

class OriginTest extends DoctrineExtensionTestCase
{
    public function data(): array
    {
        return [
            ['whatever', 'cli'],
            ['bin/phpunit', 'testing_cli@phpunit'],
            ['bin/custom', 'testing_cli@custom_string'],
            ['bin/console -e prod -vv test:symfony --no-debug', 'testing_cli@test:symfony'],
        ];
    }

    /**
     * @dataProvider data
     */
    public function test(string $command, string $expected): void
    {
        // override argv
        $argvBackup = $_SERVER['argv'];
        // fake argv
        $_SERVER['argv'] = \array_map('trim', explode(' ', $command));
        $em = self::getDoctrine()->getManagerForClass(OriginTestEntity::class);

        $entity = new OriginTestEntity();
        $em->persist($entity);
        $em->flush();

        $this->assertEquals($expected, $entity->getContextOrigin());
        // argv back to original
        $_SERVER['argv'] = $argvBackup;
    }
}
